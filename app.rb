require "bundler"
Bundler.require

Sinatra::Xsendfile.replace_send_file!

# default: X-SendFile (Apache)
set :xsf_header, 'X-Accel-Redirect' # (Nginx)

get '/' do
  "hello!"
end

get '/sendfile' do
  x_send_file(__FILE__, filename: "app.rb")
end
