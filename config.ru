require 'bundler'
Bundler.require

disable :run, :reload

require_relative 'app'

run Sinatra::Application
